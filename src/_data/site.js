module.exports = {
      pitch: {
        url: "#infographics" //"https://start9.com/latest/learn/index"
      },
      products: {
        buyUrl: "https://store.start9.com/",
        diyUrl: "https://start9.com/latest/diy",
      },
      bitcoin: {
        whyRunBitcoinUrl: "https://bitcoinmagazine.com/culture/six-reasons-you-should-run-bitcoin-node",
        whyRunLightningUrl: "",
      },
      beYourOwn: {
        url: "https://marketplace.start9.com/"
      },
      poweredBy: {
        url: "https://github.com/Start9Labs/embassy-os"
      },
      support: {
        url: "https://start9.com/latest/support/index"
      },
      dev: {
        url: "https://start9.com/latest/developer-docs/"
      },
      footer: {
        copyright: "© " + new Date().getFullYear() + " by START9 LABS, INC.",
        canary:
          "WE HAVE NEVER RECEIVED A SECRET GOVERNMENT REQUEST TO HAND OVER USER INFORMATION.",
      }
    };
